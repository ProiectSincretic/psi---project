﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulator
{

    /// <summary>
    /// Enumerare folosita pentru a transmite/receptiona comenzi
    /// </summary>
    public enum Command
    {
        Stopped = 0,
        Started = 1,
        SmallDelay = 2,        
        LongDelay = 8,  
        MediumDelay = 4,
        G1 = 32,
        G2 = 64,
        Hold= 3
    }

    /// <summary>
    /// Enumerare folosita pentru a modela starea procesului
    /// </summary>
    public enum ProcessState
    {        
        G1 = 1,
        G2 = 2,
        S1 = 4,
        S2 = 8,
        S3 = 16,
        S4 = 32,
        S5 = 64,
        Stopped = 128

    }



    public class Simulator
    {
        private byte[] _state = new byte[] { 0x00, 0x00};
        
        private void ExecuteLongDelayCommand(int Delay)
        {
            System.Threading.Thread.Sleep(Delay);
            _state[0] = (int)ProcessState.G1;
            System.Threading.Thread.Sleep(Delay);
            
            _state[0] = (int)ProcessState.G1 
                | (int)ProcessState.G2;
            System.Threading.Thread.Sleep(Delay);

            _state[0] = (int)ProcessState.G1 
                | (int)ProcessState.G2 
                | (int)ProcessState.S1;
            System.Threading.Thread.Sleep(Delay);

            _state[0] = (int)ProcessState.G1 
                | (int)ProcessState.G2 
                | (int)ProcessState.S1 
                | (int)ProcessState.S2;
            System.Threading.Thread.Sleep(Delay);

            _state[0] = (int)ProcessState.G1 
                | (int)ProcessState.G2 
                | (int)ProcessState.S1 
                | (int)ProcessState.S2 
                | (int)ProcessState.S3;
            System.Threading.Thread.Sleep(Delay);

            _state[0] = (int)ProcessState.G1 
                | (int)ProcessState.G2 
                | (int)ProcessState.S1 
                | (int)ProcessState.S2 
                | (int)ProcessState.S3 
                | (int)ProcessState.S4;
            System.Threading.Thread.Sleep(Delay);

            _state[0] = (int)ProcessState.G1 
                | (int)ProcessState.G2 
                | (int)ProcessState.S1 
                | (int)ProcessState.S2 
                | (int)ProcessState.S3 
                | (int)ProcessState.S4 
                | (int)ProcessState.S5;
            System.Threading.Thread.Sleep(Delay);
        }

        private void ExecuteShortDelayCommand(int Delay)
        {
            _state[0] = (int)ProcessState.G1
                | (int)ProcessState.G2 
                | (int)ProcessState.S1 
                | (int)ProcessState.S2 
                | (int)ProcessState.S3 
                | (int)ProcessState.S4 
                | (int)ProcessState.S5;
            System.Threading.Thread.Sleep(Delay);

            _state[0] = (int)ProcessState.G2 
                | (int)ProcessState.S1 
                | (int)ProcessState.S2 
                | (int)ProcessState.S3 
                | (int)ProcessState.S4 
                | (int)ProcessState.S5;
            System.Threading.Thread.Sleep(Delay);

            _state[0] = (int)ProcessState.S1 
                | (int)ProcessState.S2 
                | (int)ProcessState.S3 
                | (int)ProcessState.S4 
                | (int)ProcessState.S5;
            System.Threading.Thread.Sleep(Delay);

            _state[0] = (int)ProcessState.S1 
                | (int)ProcessState.S2 
                | (int)ProcessState.S3 
                | (int)ProcessState.S4;
            System.Threading.Thread.Sleep(Delay);

            _state[0] = (int)ProcessState.S1 
                | (int)ProcessState.S2 
                | (int)ProcessState.S3;
            System.Threading.Thread.Sleep(Delay);

            _state[0] = (int)ProcessState.S1
                | (int)ProcessState.S2;
            System.Threading.Thread.Sleep(Delay);

            _state[0] = (int)ProcessState.S1;
            System.Threading.Thread.Sleep(Delay);

            _state[0] = (int)ProcessState.Stopped;
            System.Threading.Thread.Sleep(Delay);
        }

        private void ExecuteMediumDelayCommand(int Delay)
        {
            _state[0] = (int)ProcessState.G1
                | (int)ProcessState.G2
                | (int)ProcessState.S1
                | (int)ProcessState.S2
                | (int)ProcessState.S3
                | (int)ProcessState.S4
                | (int)ProcessState.S5;
            System.Threading.Thread.Sleep(Delay);

            _state[0] = (int)ProcessState.G2
                | (int)ProcessState.S1
                | (int)ProcessState.S2
                | (int)ProcessState.S3
                | (int)ProcessState.S4
                | (int)ProcessState.S5;
            System.Threading.Thread.Sleep(Delay);

            _state[0] = (int)ProcessState.S1
                | (int)ProcessState.S2
                | (int)ProcessState.S3
                | (int)ProcessState.S4
                | (int)ProcessState.S5;
            System.Threading.Thread.Sleep(Delay);

            _state[0] = (int)ProcessState.S1
                | (int)ProcessState.S2
                | (int)ProcessState.S3
                | (int)ProcessState.S4;
            System.Threading.Thread.Sleep(Delay);

            _state[0] = (int)ProcessState.S1
                | (int)ProcessState.S2
                | (int)ProcessState.S3
                | (int)ProcessState.S4
                | (int)ProcessState.S5;
            System.Threading.Thread.Sleep(Delay);

            _state[0] = (int)ProcessState.S1
                | (int)ProcessState.S2
                | (int)ProcessState.S3
                | (int)ProcessState.S4;
            System.Threading.Thread.Sleep(Delay);

        }


        public void UpdateState(int CommandState)
        {            
            int longDelayForBothPump = (int)Command.LongDelay 
                | (int)Command.G1
                | (int)Command.G2 
                | (int)Command.Started;

            int shortDelayForBothPump = (int)Command.SmallDelay 
                | (int)Command.G1 
                | (int)Command.G2 
                | (int)Command.Stopped;

            int mediumDelayForBothPump = (int)Command.MediumDelay
                | (int)Command.G1
                | (int)Command.G2
                | (int)Command.Hold;

            if (CommandState == longDelayForBothPump)
            {
                ExecuteLongDelayCommand(5000);
            }
            else if (CommandState == shortDelayForBothPump)
            {
                ExecuteShortDelayCommand(2000);
            }
            else if (CommandState == mediumDelayForBothPump)
            {
                ExecuteMediumDelayCommand(3000);
            }
        }

        public byte[] GetState()
        {
            return _state;
        }
    } 
}
