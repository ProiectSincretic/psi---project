﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TCP_PLC
{
	public class Sender
	{		
		BackgroundWorker _senderWorker;
        Simulator.Simulator _process;


        public Sender(BackgroundWorker senderWorker, Simulator.Simulator Process)
		{
			_senderWorker = senderWorker;
            _process = Process;
		}

		public void Send()
		{
			try
			{
				//Se creaza un TCP client cu adresa de IP si portul 
				TcpClient client = new TcpClient("127.0.0.1", 3000);
               
         //       _senderWorker.ReportProgress(0, string.Format("Apasati butonul S5(START) ca sa incarcati silozul \n"));
               
                while (true)
				{
					NetworkStream nwStream = client.GetStream();
					byte[] bytesToSend = new byte[6];
                    for(int i=0;i<5;i++)
					bytesToSend[i] = _process.GetState()[i];					

					//se apeleaza metoda report progress pentru a face update pe UI (adica in aplicatia consola).
					_senderWorker.ReportProgress(0, string.Format("{0} {1} {2} {3} {4}", bytesToSend[0].ToString(), bytesToSend[1].ToString(), bytesToSend[2].ToString(),
                        bytesToSend[3].ToString(), bytesToSend[4].ToString()));
					nwStream.Write(bytesToSend, 0, bytesToSend.Length);
                    

                    Thread.Sleep(2000);
				}
			}
			catch (Exception ex)
			{
				_senderWorker.ReportProgress(0, ex.ToString());
			}
			
		}
	}
}
