﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulator
{

    public class Simulator
    {
        private byte[] _state = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00};

        private void ExecuteS5Command(int Delay)
        {
            while(_state[0] < 100)
            {
                System.Threading.Thread.Sleep(Delay);
                _state[0] += 1;
            }
        }

        private void ExecuteS0Command(int Delay)
        {
            bool scadere = true;

            while (scadere == true && _state[0] > 20)
            {

                System.Threading.Thread.Sleep(Delay);
                _state[0] -= 1;
                if (_state[0] <=20)
                {
                    _state[3] = 0;
                    Delay = (20000 / _state[2]);
                    scadere = false;
                }
            }
            while (scadere == false && _state[0] < 100)
            {
                System.Threading.Thread.Sleep(Delay);
                _state[0] += 1;
                if (_state[0] > 100)
                {
                    scadere = true;
                }
            }
        }

        public void UpdateState(int CommandState, int pot1, int pot2, int onOFF)
        {
            int delayS5 = (20000 / pot1);
            int delayS0;

            //-------
            _state[2] = (byte)pot1;
            _state[3] = (byte)pot2;
            //-------
            if (onOFF == 1)
            {
                switch (CommandState)
                {
                    case 1:
                        _state[1] = 1;
                        ExecuteS5Command(delayS5);
                        break;
                    case 2:
                        _state[1] = 2;
                        if (pot2 > pot1)
                        {
                            delayS0 = 20000 / (pot2 - pot1);
                            ExecuteS0Command(delayS0);
                        }
                        else if(pot2<pot1)
                        {
                            delayS0 = 20000 / (pot1 - pot2);
                            ExecuteS5Command(delayS0);
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        public byte[] GetState()
        {
            _state[4] = (byte)((int)_state[0] / 20);
            return _state;
        }
    } 
}
