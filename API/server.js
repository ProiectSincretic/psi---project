
const cors = require("cors");
const knex = require('knex')
var net = require('net');

const express = require('express');
const bodyParser = require("body-parser");
const app = express();
const http = require('http');
var socketIO = require('socket.io');
const cazan = require('./controllers/cazan');
var lastState = [0,0,0,0, 0 , 0];


const db = knex({
    client: 'pg',
    connection: {
        connectionString: 'postgres://miilaptrihexmg:de5ed4ccb8075049c763484adb28a6863d9957877814a762c4750c0fc189854b@ec2-54-247-85-251.eu-west-1.compute.amazonaws.com:5432/d76207knegvg3p',
        ssl: true
    }
});

const corsOptions = {
    origin: 'http://localhost:4000',
    optionsSuccessStatus: 200
}

var net = require('net');
var client = new net.Socket();

app.use(cors(corsOptions));

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.get('/getAllData', cazan.handleCazanGet(db))
app.post('/optS0', cazan.handleS0(db, client))
app.post('/optS5', cazan.handleS5(db, client))
app.post('/connectToSim', cazan.connectToSim(db, client))

const server = http.createServer(app);
const io = socketIO(server);


server.listen(3005, () => {
    console.log('port for api calls is 3005');
});

io.on('connection', socket => {
    console.log('User connected')
    
    socket.on('disconnect', () => {
      console.log('user disconnected')
    })
  })


  function arraysEqual(arr1, arr2) {
    if (arr1.length !== arr2.length)
        return false;
    for(var i = arr1.length; i>1 ;i--) {
        if(arr1[i] !== arr2[i])
            return false;
    }

    return true;
  }

var saveStateInDB= (data) =>{
  if (!arraysEqual(data, lastState)){
    lastState = data;
    cazan.saveDBState(db, data);
  }
};

var sendDataToFrontend = (data) => { 
    io.emit('soc', {"result": data});
}

var receiver = net.createServer(function(socket) {
    socket.on('data', function(data){
		console.log(data);
        textChunk = JSON.stringify(data);
        text = JSON.parse(textChunk);
        console.log("MY DATA",text.data);
       saveStateInDB(text.data);
       sendDataToFrontend(text.data);
	});
});

receiver.listen(3000, '127.0.0.1');

