const handleCazanGet = (db) => (req, res) => {
    db.select('*').from('cazan')
      .then(data => {
        if (data.length) {
          res.json(data)
        } else {
          res.status(400).json('Not found')
        }
      })
      .catch(err => res.status(400).json('error getting user'))
}



const connectToSim= (db, client) => (req, res) => {
  client.connect(2000, '127.0.0.1', function() {
    res.status(200);
  });
  db('cazan')
  .whereNot('id', 0)
  .del()
  .then(data => {
    res.status(200).send("connected");
  })
}

const handleS0 = (db, client) => (req, res) => {
    const {pot1, pot2, onOff} = req.body;
    var array = new Uint8Array(100);
    array[0] = 2;
    array[1] = pot1;
    array[2] = pot2;
    array[3] = onOff;
    client.write(Buffer.from(array));
    res.status(200).send("sended S0");
}

const handleS5 = (db, client) => (req, res) => {
  const {pot1, pot2, onOff} = req.body;
  var array = new Uint8Array(100);
  array[0] = 1;
  array[1] = pot1;
  array[2] = pot2;
  array[3] = onOff;
  client.write(Buffer.from(array));
  res.status(200).send("sended S5");
}

const saveDBState = (db, data) => {


  db('cazan')
    .returning('*')
    .insert({
          level : data[0],
          command : data[1],
          pot1 : data[2],
          pot2 : data[3],
          created_on: new Date()

    })
    .then(createdCazan => {
        console.log(createdCazan);      
    })

}

module.exports = {
    handleCazanGet: handleCazanGet,
    handleS0: handleS0,
    handleS5: handleS5,
    connectToSim :connectToSim,
    saveDBState : saveDBState

}


