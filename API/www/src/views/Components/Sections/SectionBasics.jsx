import React from "react";
// plugin that creates slider
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// @material-ui/icons
// core components
import socketIOClient from "socket.io-client";
import axios from 'axios';
import Button from "@material-ui/core/Button";
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableRow from '@material-ui/core/TableRow';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import { Line } from 'react-chartjs-2';
/* import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts'; */
import basicsStyle from "assets/jss/material-kit-react/views/componentsSections/basicsStyle.jsx";
let image = require("assets/img/cazanel.PNG");

class SectionBasics extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      endpoint: "http://localhost:3005",
      dataFromSocket: [0, 0, 0, 0, 0, 0, 0, 0],
      timesChanged: 0,
      data: {
        labels: [

        ],
        datasets: [
          {
            label: 'Nivel',
            data: [],
            fill: false,          // Don't fill area under the line
            borderColor: 'green'  // Line color
          }
        ]
      },
      barHeight: 17,
      connected: false,
      inaltimeNivelSiloz: 2,
      butonNivel: 64.35,
      dataTable: []

    };

  }
  componentDidMount = () => {
    let socket = socketIOClient('http://localhost:3005/');
    socket.on('soc', col => {
      this.reloadData(col);
    })
    axios.get('http://localhost:3005/getAllData'
    ).then(list => {
      this.setState({
        dataTable: list.data
      })
    })
  }

  da = () => {
    this.setState({
      barHeight: 40
    })
  }

  s0Handler = () => {
    axios.post('http://localhost:3005/optS0/', {
      "pot1": Number(document.getElementById("potentiometru1").value),
      "pot2": Number(document.getElementById("potentiometru2").value),
      "onOff": 1
    }, {
        headers: { "Access-Control-Allow-Origin": "*" },
        withCredentials: false
      })
      .then(list => {
      })
  }

  s5Handler = () => {
    axios.post('http://localhost:3005/optS5/', {
      "pot1": Number(document.getElementById("potentiometru1").value),
      "pot2": Number(document.getElementById("potentiometru2").value),
      "onOff": 1
    })
      .then(list => {
      })
  }

  connectHandler = () => {
    axios.post('http://localhost:3005/connectToSim/')
      .then(list => {
      })

    this.setState({
      connected: true
    })
  }

  reloadData = (col) => {
    let newCazan = this.state.data.datasets[0].data;
    let newLabels = this.state.data.labels;
    newCazan.push(col.result[4]);
    newLabels.push((new Date).toLocaleTimeString());
    let newData = { ...this.state.data };
    newData.datasets[0].data = newCazan;
    let nivelButon = 64.35;
    if (col.result[4] === 1) {
      nivelButon = 60.45;
    }
    if (col.result[4] === 2) {
      nivelButon = 56.6;
    }
    if (col.result[4] === 3) {
      nivelButon = 52.45;
    }
    if (col.result[4] === 4) {
      nivelButon = 48.70;
    }
    if (col.result[4] === 5) {
      nivelButon = 44.75;
    }
    this.setState({
      dataFromSocket: col.result,
      timesChanged: this.state.timesChanged + 1,
      data: newData,
      inaltimeNivelSiloz: (col.result[0] / 100) * 220,
      butonNivel: nivelButon
    })
  }
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.sections}>
        <div className={classes.container}>
          <div className={classes.title} style={{ marginBottom: 30 }}>
            <h2>Functionare siloz in timp real</h2>
          </div>
          <Grid container>
            <Grid item xs={6}>
              <Grid container spacing={16}>
                <Grid item xs={12}>
                  <h3>Panou Control</h3>
                  <h5>{this.state.dataFromSocket[0]} : {this.state.dataFromSocket[1]} : {this.state.dataFromSocket[2]} : {this.state.dataFromSocket[3]} : {this.state.dataFromSocket[4]} : {this.state.dataFromSocket[5]} - {this.state.timesChanged}</h5>
                </Grid>
                <Grid item xs={12}>
                  <Button variant="contained" color="default" className={classes.button} onClick={this.connectHandler}><b>Conectare</b></Button>
                </Grid>
                <Grid item xs={12}>
                  <input type="text" id="potentiometru1" placeholder="potentiometru 1"></input>
                </Grid>
                <Grid item xs={12}>
                  <input type="text" id="potentiometru2" placeholder="potentiometru 2"></input>
                </Grid>
                <Grid item xs={12}>
                  <Button variant="contained" color="default" className={classes.button} onClick={this.s0Handler}>S0</Button>
                </Grid>
                <Grid item xs={12}>
                  <Button variant="contained" color="default" className={classes.button} onClick={this.s5Handler}>S5</Button>
                </Grid>
                <Grid item xs={6}>
                  
                </Grid>
                <Grid item xs={3}>
                  <Button variant="contained" color="secondary" className={classes.button} >{this.state.dataFromSocket[2]}</Button>
                </Grid>
                <Grid item xs={3}> {
                  this.state.dataFromSocket[3] === 0 ?
                  <Button variant="contained" color="default" className={classes.button} >{this.state.dataFromSocket[3]}</Button>
                  :<Button variant="contained" color="secondary" className={classes.button} >{this.state.dataFromSocket[3]}</Button>
                  
                }
                  
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={6}>
              <div style={{ backgroundImage: "url(" + image + ")", width: 650, height: 450, backgroundRepeat: "no-repeat", display: "flex", alignItems: "flex-end" }}>
                <div style={{ width: 101, height: 220, marginLeft: "30%", marginBottom: "25%", display: "flex", alignItems: "flex-end" }}>
                  <div style={{ backgroundColor: "blue", width: 101, height: this.state.inaltimeNivelSiloz }}></div>
                </div>
                <div style={{ backgroundColor: "red", width: 20, height: 20, marginLeft: "32.70%", marginBottom: this.state.butonNivel + "%", borderRadius: "10px" }}></div>
              </div>
            </Grid>
          </Grid>
          <Card className={classes.card}>
            <CardContent>
              <h3>Command History</h3>
              <Table >
                <TableHead>
                  <TableRow>
                    <TableCell>Level</TableCell>
                    <TableCell>Command</TableCell>
                    <TableCell>TimeStamp</TableCell>
                    <TableCell>Potentiometru 1</TableCell>
                    <TableCell>Potentiometru 2</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {
                    this.state.dataTable.map((value, index) => {
                      return (<TableRow key={value.id} >
                        <TableCell>{value.level}</TableCell>
                        <TableCell>{value.command}</TableCell>
                        <TableCell>{value.created_on}</TableCell>
                        <TableCell>{value.pot1}</TableCell>
                        <TableCell>{value.pot2}</TableCell>
                      </TableRow>)
                    })
                  }
                </TableBody>
              </Table>
            </CardContent>
          </Card>
          <Card className={classes.card} style={{ marginTop: 40 }}>
            <CardContent>
              <div key={this.state.data.labels[0]}>
                <h3>Grafic in timp real al nivelului silozului</h3>
                <Line data={this.state.data} redraw />
              </div>
            </CardContent>
          </Card>
        </div>
      </div>
    );
  }
}

export default withStyles(basicsStyle)(SectionBasics);
